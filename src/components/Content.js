import React, {Component} from "react";
import {Form, Button, Input, Icon, Row, Col, List} from 'antd';

class Content extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputTodo: "",
            todoList: [
                'Brush your teeth',
                'Breakfast',
                'Go to Office'
            ]
        }
    }

    inputChange = (event) => {
        console.log('event', event.target.value);
        this.setState({inputTodo: event.target.value});

    };
    //ES6 - Fat Arrow Function

    //Modern ES6 way
    // inputChange = event => this.setState({inputTodo: event.target.value});

    // AddTodo in a state
    addTodo = event => {
        event.preventDefault();

        if(this.state.inputTodo === ""){
            alert("Input todo is empty");
            return null;
        }

        let temp = this.state.todoList; // Todolist array
        temp.push(this.state.inputTodo); //Inputtodo push

        this.setState({todoList: temp, inputTodo: ""});

    };



    render() {
        console.log('From render Function', this.state);

        return (
            <div>
                <Row style={{marginTop: '20px'}}>
                    <Col md={6} sm={24}></Col>
                    <Col md={12} sm={24}>
                        <Form>
                        <Form.Item>
                            <Input value={this.state.inputTodo} onChange={this.inputChange} name={"todo"} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="todo" />
                        </Form.Item>

                        <Button onClick={this.addTodo} type="primary" htmlType="submit" className="login-form-button">
                            Add Todo
                        </Button>
                    </Form></Col>
                    <Col md={6} sm={24}></Col>
                 </Row>

                <Row style={{marginTop: '20px'}}>
                    <Col md={6} sm={24}></Col>
                    <Col md={12} sm={24}>
                        <List
                            header={<div><b>My Todo List</b></div>}
                            bordered
                            dataSource={this.state.todoList}
                            renderItem={item => (<List.Item>{item}</List.Item>)}
                        />
                     </Col>
                    <Col md={6} sm={24}></Col>
                </Row>
            </div>
        );
    }
}
export  default Content;

