import React, {Component} from 'react'; // {} implies destructuring. ES6 new feature
import {Menu, Icon} from 'antd';
import 'antd/dist/antd.css';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class Header extends Component {

    render() {
        return(
            <Menu
                mode="horizontal"
            >
                <Menu.Item key="mail">
                    <Icon type="mail" />Home
                </Menu.Item>
                <Menu.Item key="about">
                    <Icon type="mail" />About
                </Menu.Item>
                <Menu.Item key="contact">
                    <Icon type="mail" />Contact
                </Menu.Item>
            </Menu>
        );
    }


}

export default Header;