import React, { Component } from 'react';
import NavHeader from './components/Header';
import Footer from './components/Footer';
import Content from './components/Content';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavHeader/>
        <Content/>
        <Footer/>
      </div>
    );
  }
}

export default App;
